package pl.jstk.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import pl.jstk.constants.ViewNames;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private UserController userController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setViewResolvers(new InternalResourceViewResolver("templates", null))
                .build();
    }

    @Test
    public void testLoginPage() throws Exception {
        // given when
        ResultActions resultActions = mockMvc.perform(get("/login"));
        // then
        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.LOGIN))
                .andDo(print())
                .andExpect(content().string(containsString("")));
    }

    @Test
    public void testAccessDeniedPage() throws Exception {
        // given when
        ResultActions resultActions = mockMvc.perform(get("/books/accessDenied"));
        // then
        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.ERROR_403))
                .andDo(print())
                .andExpect(content().string(containsString("")));
    }


}