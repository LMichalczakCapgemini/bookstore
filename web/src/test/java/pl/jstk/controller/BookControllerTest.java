package pl.jstk.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import pl.jstk.constants.ViewNames;
import pl.jstk.enumerations.BookStatus;
import pl.jstk.service.BookService;
import pl.jstk.to.BookTo;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;
import static java.lang.Math.toIntExact;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.doAnswer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class BookControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private BookController bookController;

    @Mock
    private BookService bookService;

    private List<BookTo> booksRepo;
    private List<BookTo> findByAuthorResult;
    private List<BookTo> findByTitleResult;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(bookController)
                .setViewResolvers(new InternalResourceViewResolver("templates", null))
                .build();

        booksRepo = resetBooksRepo();
        findByTitleResult = new LinkedList<>();
        findByAuthorResult = new LinkedList<>();

        given(bookService.findAllBooks()).willReturn(booksRepo);
        given(bookService.findBooksByAuthor("Janusz Jankowski")).willReturn(findByAuthorResult);
        given(bookService.findBooksByTitle("Third book")).willReturn(findByTitleResult);
        given(bookService.findBookById(Mockito.anyLong())).willReturn(booksRepo.get(0));
        doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                booksRepo.add((BookTo) args[0]);
                return null;
            }})
                .when(bookService).saveBook(Mockito.any(BookTo.class));
        doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                booksRepo.remove(toIntExact((Long) args[0])-1);
                return null;
            }})
                .when(bookService).deleteBook(Mockito.anyLong());
    }

    @Test
    public void testBooks() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/books"));

        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.BOOKS))
                .andDo(print())
                .andExpect(model().attribute("bookList", booksRepo))
                .andExpect(content().string(containsString("")));
    }

    @Test
    public void testBook() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/books/book?id=1"));

        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.BOOK))
                .andDo(print())
                .andExpect(model().attribute("book", booksRepo.get(0)))
                .andExpect(content().string(containsString("")));

    }

    @Test
    public void testAddBook() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/books/addBook"));

        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.ADD_BOOK))
                .andDo(print())
                .andExpect(model().attribute("newBook", new BookTo()))
                .andExpect(content().string(containsString("")));
    }

    @Test
    public void testAddBookResult() throws Exception {
        ResultActions resultActions = mockMvc.perform(post("/books/addBookResult")
        .param("title", "Nowy tytul").param("authors", "Autorzy")
                .param("status", "FREE"));

        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.BOOKS))
                .andDo(print())
                .andExpect(model().attribute("bookList", booksRepo))
                .andExpect(content().string(containsString("")));
        assertThat(booksRepo.size(),is(equalTo(6)));
    }

    @Test
    public void testSearchBook() throws Exception{
        ResultActions resultActions = mockMvc.perform(get("/books/searchBook"));

        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.SEARCH_BOOK))
                .andDo(print())
                .andExpect(model().attribute("filterBook", new BookTo()))
                .andExpect(content().string(containsString("")));
    }

    @Test
    public void testSearchBookResultSuccess() throws Exception{
        findByAuthorResult.add(booksRepo.get(2));
        findByTitleResult.add(booksRepo.get(2));

        ResultActions resultActions = mockMvc.perform(get("/books/searchResult")
        .param("title", "Third book").param("authors", "Janusz Jankowski"));

        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.BOOKS))
                .andDo(print())
                .andExpect(model().attribute("bookList", resetBooksRepo().subList(2,3)))
                .andExpect(content().string(containsString("")));
    }

    @Test
    public void testSearchBookResultFail() throws Exception{
        findByAuthorResult.add(booksRepo.get(0));
        findByTitleResult.add(booksRepo.get(2));

        ResultActions resultActions = mockMvc.perform(get("/books/searchResult")
                .param("title", "Third book").param("authors", "Jan Kowalski"));

        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.NO_MATCHING_BOOK_FOUND_INFO))
                .andDo(print())
                .andExpect(content().string(containsString("")));
    }

    @Test
    public void testDeleteBookSuccess() throws Exception{
        ResultActions resultActions = mockMvc.perform(get("/books/deleteBook")
        .param("id", "5"));

        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.BOOK_REMOVED_INFO))
                .andDo(print())
                .andExpect(content().string(containsString("")));
    }

    @Test
    public void testDeleteBookFail() throws Exception{
        mockMvc.perform(get("/books/deleteBook")
                .param("id", "5"));
        ResultActions resultActions = mockMvc.perform(get("/books/deleteBook")
                .param("id", "5"));

        resultActions.andExpect(status().isOk())
                .andExpect(view().name(ViewNames.BOOK_NOT_REMOVED_INFO))
                .andDo(print())
                .andExpect(content().string(containsString("")));
    }

    private List<BookTo> resetBooksRepo(){
        List<BookTo> booksRepo = new LinkedList<>();
        booksRepo.add(new BookTo(1L, "First book", "Jan Kowalski", BookStatus.FREE));
        booksRepo.add(new BookTo(2L, "Second book", "Zbigniew Nowak", BookStatus.FREE));
        booksRepo.add(new BookTo(3L, "Third book", "Janusz Jankowski", BookStatus.FREE));
        booksRepo.add(new BookTo(4L, "Starter kit book", "acper Ossoliński", BookStatus.FREE));
        booksRepo.add(new BookTo(5L, "Z kamerą wśród programistów", "Krystyna Czubówna", BookStatus.MISSING));
        return booksRepo;
    }
}

