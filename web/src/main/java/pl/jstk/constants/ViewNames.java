package pl.jstk.constants;

import java.io.Serializable;

public final class ViewNames implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String BOOKS = "books";
	public static final String LOGIN = "login";
	public static final String WELCOME = "welcome";
	public static final String ADD_BOOK = "addBook";
	public static final String BOOK = "book";
	public static final String ERROR_403 = "info/403";
	public static final String SEARCH_BOOK = "searchBook";
	public static final String NO_MATCHING_BOOK_FOUND_INFO = "info/noMatchingBookFound";
	public static final String BOOK_REMOVED_INFO = "info/bookRemoved";
	public static final String BOOK_NOT_REMOVED_INFO = "info/bookNotRemoved";
}
