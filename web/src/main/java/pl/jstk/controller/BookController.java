package pl.jstk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.jstk.constants.ViewNames;
import pl.jstk.enumerations.BookStatus;
import pl.jstk.service.BookService;
import pl.jstk.to.BookTo;

import java.util.List;

@Controller
@RequestMapping("/books")
public class BookController {
//TODO search book na strone books
    @Autowired
    private BookService bookService;

    @GetMapping(value = "")
    public String showAllBooks(Model model){
        model.addAttribute("bookList", bookService.findAllBooks());
        return ViewNames.BOOKS;
    }

    @GetMapping(value = "book")
    public String showBookDetails(Model model, @RequestParam("id") Long id){
        model.addAttribute("book", bookService.findBookById(id) );
        return ViewNames.BOOK;
    }

    @GetMapping(value = "/addBook")
    public String addBook(Model model){
        model.addAttribute("newBook", new BookTo());
        return ViewNames.ADD_BOOK;
    }

    @PostMapping(value = "/addBookResult")
    public String addBookResult(Model model, @RequestParam("title") String title,
                                @RequestParam("authors") String authors,
                                @RequestParam("status") BookStatus bookStatus) {
        BookTo newBook = new BookTo();
        newBook.setTitle(title);
        newBook.setAuthors(authors);
        newBook.setStatus(bookStatus);

        bookService.saveBook(newBook);
        model.addAttribute("bookList", bookService.findAllBooks());
        return ViewNames.BOOKS;
    }

    @GetMapping(value = "/searchBook")
    public String search(Model model){
        model.addAttribute("filterBook", new BookTo());
        return ViewNames.SEARCH_BOOK;
    }

    @GetMapping(value = "/searchResult")
    public String searchResult(Model model, @RequestParam("title") String title,
                               @RequestParam("authors") String authors){
        List<BookTo> finalSearch = bookService.findAllBooks();
        //TODO findByStatus

        if(!authors.isEmpty()) {
            List<BookTo> byAuthors = bookService.findBooksByAuthor(authors);
            finalSearch.retainAll(byAuthors);
        }

        if(!title.isEmpty()) {
            List<BookTo> byTitle = bookService.findBooksByTitle(title);
            finalSearch.retainAll(byTitle);
        }
        
        if(finalSearch.isEmpty())
            return ViewNames.NO_MATCHING_BOOK_FOUND_INFO;

        model.addAttribute("bookList", finalSearch);
        return ViewNames.BOOKS;
    }

    @GetMapping(value = "/deleteBook")
    public String deleteBook(@RequestParam("id") Long id){
        try {
            bookService.deleteBook(id);
        }
        catch (Exception ex){
            return ViewNames.BOOK_NOT_REMOVED_INFO; //TODO inna strona z informacja
        }
        return ViewNames.BOOK_REMOVED_INFO;
    }
}
