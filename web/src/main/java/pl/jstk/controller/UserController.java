package pl.jstk.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.jstk.constants.ViewNames;

@Controller
public class UserController {
//TODO ogarnac error
    @RequestMapping(value = "/login")
    public String login() {
        return ViewNames.LOGIN;
    }

    @RequestMapping(value = "/books/accessDenied")
    public String add403() {
        return ViewNames.ERROR_403;
    }
}
