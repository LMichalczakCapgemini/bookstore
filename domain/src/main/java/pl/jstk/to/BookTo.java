package pl.jstk.to;

import pl.jstk.enumerations.BookStatus;

import java.util.Objects;

public class BookTo {
    private Long id;
    private String title;
    private String authors;
    private BookStatus status;
    
    public BookTo() {
    }

    public BookTo(Long id, String title, String authors, BookStatus status) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.setStatus(status);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

	public BookStatus getStatus() {
		return status;
	}

	public void setStatus(BookStatus status) {
		this.status = status;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookTo)) return false;
        BookTo bookTo = (BookTo) o;
        return Objects.equals(getId(), bookTo.getId()) &&
                Objects.equals(getTitle(), bookTo.getTitle()) &&
                Objects.equals(getAuthors(), bookTo.getAuthors()) &&
                getStatus() == bookTo.getStatus();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getTitle(), getAuthors(), getStatus());
    }
}
